import axios from 'axios';
import {API_URL} from "../constants/api-domain";

export function notifyOverwatch(message, id){
  // console.log('here');
  axios.post(`${API_URL}/api/exam/notify`, {
    user_id: id,
    description: message,
  })
  .then((res) => {
    console.log(res);
    return res
  })
  .catch((error) => {
    console.log(error);
  });
}
