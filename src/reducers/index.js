import {
  ADD_QUESTION,
  NEXT_QUESTION,
  PREV_QUESTION,
  TO_QUESTION,
  CREATE_QUESTION_VISITED,
  SAVE_PROFILE,
  TOGGLE_ANSWERING,
  START_EXAM,
  CREATE_ANSWERS,
} from '../constants/action-types';

const initialState = {
  questions: [],
  questionVisited: [],
  answers: [],
  questionIndex: 0,
  sectionIndex: 0,
  profile: {},
  answering: true,
};

function rootReducer(state = initialState, action) {
  // console.log(action.payload);
  let temp = {};

  if (action.type === ADD_QUESTION) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === NEXT_QUESTION) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === PREV_QUESTION) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === TO_QUESTION) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === CREATE_QUESTION_VISITED) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === CREATE_ANSWERS) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === SAVE_PROFILE) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === TOGGLE_ANSWERING) {
    return Object.assign(temp, state, action.payload);
  }

  if (action.type === START_EXAM) {
    return Object.assign(state, action.payload);
  }

  return state;
}

export default rootReducer;
