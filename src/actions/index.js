import {
  ADD_QUESTION,
  NEXT_QUESTION,
  PREV_QUESTION,
  TO_QUESTION,
  CREATE_QUESTION_VISITED,
  SAVE_PROFILE,
  TOGGLE_ANSWERING,
  START_EXAM,
  CREATE_ANSWERS,
} from '../constants/action-types';

export function addQuestion(payload) {
  return { type: ADD_QUESTION, payload };
}

export function nextQuestion(payload) {
  return { type: NEXT_QUESTION, payload };
}

export function prevQuestion(payload) {
  return { type: PREV_QUESTION, payload };
}

export function toQuestion(payload) {
  return { type: TO_QUESTION, payload };
}

export function createQuestionVisited(payload) {
  return { type: CREATE_QUESTION_VISITED, payload };
}

export function saveProfile(payload) {
  return { type: SAVE_PROFILE, payload };
}

export function toggleAnswering(payload) {
  return { type: TOGGLE_ANSWERING, payload };
}

export function startExam(payload) {
  return { type: START_EXAM, payload };
}

export function createAnswers(payload) {
  return { type: CREATE_ANSWERS, payload };
}
