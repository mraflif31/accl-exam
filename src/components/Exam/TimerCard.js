/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { AsyncStorage } from 'AsyncStorage';
import './index.css';
import { Card, Image, Button } from 'react-bootstrap';

export default class TimerCard extends Component {
  constructor(props){
    super(props);

    this.state = {
      done: false,
    };
  }

  render() {
    return (
      <Card className="card2 mt-2">
        <Card.Body className="card-body2">
          <div className="card-text2">
            <Image src={require('../../assets/images/logo_egs.png')} className="school-logo" />
            <p className="text-warning font-weight-bold">{this.props.hours} : {this.props.minutes} : {this.props.seconds}</p>
            <Button className="petunjuk-button" variant="outline-secondary">Petunjuk Pengerjaan</Button>
          </div>
        </Card.Body>
      </Card>
    );
  }
}
