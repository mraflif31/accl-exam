/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import './index.css';
import {
  Card, Button, Modal, Container, Row, Col,
} from 'react-bootstrap';
import {
  Redirect,
} from 'react-router-dom';

export default class NumberCard extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      show1: false,
      redirect: false,
    };
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleConfirmation = this.handleConfirmation.bind(this);
  }

  handleShow(state) {
    this.setState({ [state]: true });
  }

  handleClose(state) {
    this.setState({ [state]: false });
  }

  handleConfirmation() {
    this.setState({ show: false });
    this.setState({ show1: true });
  }

  processQuestion(questions) {
    const arr = [];
    questions.forEach((item) => {
      const temp = [];
      item.forEach((obj) => {
        temp.push({
          visited: false,
        });
      });
      arr.push(temp);
    });
    return arr;
  }

  render() {
    if (this.state.redirect) {
      return (<Redirect exact to="/" />);
    }
    const { processQuestion } = this;
    const { toQuestion, questions } = this.props;
    return (
      <Card className="card4">
        <Card.Body className="card-body4">
          <div className="card-text4">
            {
              processQuestion(questions).map((item, index) => (
                <div className="mb-2">
                  <p className="ml-1 text-muted">
                    Bag
                    {' '}
                    {index + 1}
                  </p>
                  <div className="exam-button-div">
                    <div
                      className="exam-button-div2"
                    >
                      {
                        item.map((obj, i) => (
                          <Button
                            className="exam-number-button"
                            variant="secondary"
                            onClick={() => toQuestion(index, i)}
                          >
                            {i + 1}
                          </Button>
                        ))
                      }
                    </div>
                  </div>
                </div>
              ))
            }
          </div>
        </Card.Body>
        <Button className="selesai-button" onClick={() => this.handleShow('show')}>
          SELESAI
        </Button>
        <Modal show={this.state.show} centered>
          <Modal.Body>
            <Container>
              <Row className="align-items-center">
                <Col xs={4} md={4} className="text-right pr-0 text-black-50">
                  Soal Diisi:
                </Col>
                <Col className="text-warning text-left pr-0" xs={2} md={2}>
                  <p style={{ fontSize: 25, color: 'limegreen' }} className="mb-0">25</p>
                </Col>
                <Col xs={4} md={4} className="text-right px-0 text-black-50">
                  Soal Kosong:
                </Col>
                <Col className="text-secondary text-left pr-0" xs={2} md={2}>
                  <p style={{ fontSize: 25, color: 'slategray' }} className="mb-0">2</p>
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={12} className="text-center" style={{ fontSize: 25 }}>
                  <p style={{ color: 'lightslategrey' }} className="my-1">00 : 03 : 12</p>
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={12} className="text-center mt-3">
                  <p className="font-italic" style={{ fontSize: 12 }}>
                    <b>Yakin sudah selesai mengerjakan Ujian?</b>
                  </p>
                </Col>
              </Row>
              <Row>
                <Col xs={6} md={6} className="text-right">
                  <Button variant="outline-primary" onClick={() => this.handleConfirmation()}>
                    Ya
                  </Button>
                </Col>
                <Col xs={6} md={6} className="text-left">
                  <Button variant="outline-secondary" onClick={() => this.handleClose('show')}>
                    Tidak
                  </Button>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
        <Modal show={this.state.show1} centered>
          <Modal.Body>
            <Container>
              <Row>
                <Col className="text-center mt-2">
                  <p className="mb-0">Selamat! Kamu sudah menyelesaikan ujian. Untuk melihat hasil ujian, silakan klik link berikut</p>
                </Col>
              </Row>
              <Row>
                <Col className="text-center">
                  <a className="btn text-info" href="http://facebook.com">bit.ly/bitbitlyly</a>
                </Col>
              </Row>
              <Row>
                <Col className="text-center mt-5">
                  <Button variant="outline-secondary" onClick={() => this.setState({ redirect: true })}>Close</Button>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
      </Card>
    );
  }
}
