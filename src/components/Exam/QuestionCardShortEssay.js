import React, { Component } from 'react';
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';
import './index.css';
import {
  Card, Col, Row, Button, Form,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleAnswering } from '../../actions';
import MathJax from 'react-mathjax-preview';

class QuestionCardShortEssay extends Component {
  constructor(props) {
    super(props);

    const tempBufferData = [];
    // const tempBufferRequest = [];

    props.data.answerData.forEach((obj) => {
      tempBufferData.push(obj);
      // tempBufferRequest.push(null);
    });

    this.node = React.createRef()

    // console.log('construct lagi ?');

    this.state = {
      data: props.data,
      bufferData: tempBufferData,
      // sending: false,
      // bufferRequest: tempBufferRequest,
    };

    this.sendAnswer = this.sendAnswer.bind(this);
  }

  // handleUpdateAnswer(obj, index) {
  //   // const {updateAnswer} = this.props;
  //   const { bufferRequest } = this.state;

  //   // console.log('handel');

  //   const tempBufferRequest = bufferRequest;
  //   tempBufferRequest[index] = obj;
  //   this.setState({
  //     bufferRequest: tempBufferRequest,
  //   });
  // }

  renderLabel(field) {
    const labels = [];
    for (let i = 0; i < field; i += 1) {
      labels.push({
        value: i,
        id: `label${i}`,
      });
    }
    return labels;
  }

  static getDerivedStateFromProps(props, state) {
    if (props.data !== state.data) {
      // console.log('props cahnged essay');
      // console.log(props.data.answerData);
      // console.log(state.data.answerData);
      // let tempSendArray = [];
        const tempBufferData = [];

      props.data.answerData.forEach((obj) => {
        tempBufferData.push(obj);
        // tempBufferRequest.push(null);
      });
      return {
        data: props.data,
        bufferData: tempBufferData,
        // sending: false,
        // bufferRequest: tempBufferRequest,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.data.sectionIndex != this.props.data.sectionIndex
      || prevProps.data.questionIndex != this.props.data.questionIndex) {
      window.MathJax.Hub.Queue([
        "Typeset",
        window.MathJax.Hub,
        this.node.current
      ]);
    // console.log('updateee')
    // console.log(this.state)
    // console.log(this.props)
      const tempBufferData = [];

      this.props.data.answerData.forEach((obj) => {
        tempBufferData.push(obj);
        // tempBufferRequest.push(null);
      });
      this.setState({
        bufferData: tempBufferData
      })
      clearInterval(this.shortEssayInterval);
      this.sendAnswer(prevProps.data.sectionIndex,
        prevProps.data.questionIndex,
        prevProps.data.questionData.q_clozes_answer_hidden,
        prevState.bufferData,
        prevProps.data.questionData.id
      );
      // this.forceUpdate()

      this.shortEssayInterval = setInterval(() => {
        this.sendAnswer();
      }, 10000);
    }
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   // console.log('essaydidupdate');
  //   const { sending, bufferRequest } = this.state;
  //   const { updateAnswer } = this.props;
  //   for (let i = 0; i < prevState.data.answerData.length; i += 1) {
  //     if (bufferRequest[i] != null && sending != true) {
  //       const reqBuff = bufferRequest[i];
  //       const tempBuff = bufferRequest;
  //       // let tempSending = sending;
  //       tempBuff[i] = null;
  //       // tempSending[i] = true;

  //       this.setState({
  //         bufferRequest: tempBuff,
  //         sending: true,
  //       });
  //       // console.log('update')
  //       // console.log('req update again after update')
  //       // console.log(reqBuff)
  //       // console.log('buffreq')
  //       // console.log(bufferRequest)
  //       // console.log(sending)
  //       updateAnswer(reqBuff);
  //       break;
  //     }
  //   }
  //   // if (bufferRequest !== null && !sending) {
  //   //   const buff = bufferRequest;
  //   //   this.setState({
  //   //     bufferRequest: null,
  //   //     sending: true,
  //   //   })
  //   //   updateAnswer(bufferRequest);
  //   // }
  // }

  componentDidMount() {
    const { toggleAnswering } = this.props;
    const { sendAnswer } = this;
    toggleAnswering({ answering: true });

    this.shortEssayInterval = setInterval(() => {
      sendAnswer();
    }, 10000);
    window.MathJax.Hub.Queue([
      "Typeset",
      window.MathJax.Hub,
      this.node.current
    ]);
  }

  componentWillUnmount() {
    // console.log('unmount success')
    clearInterval(this.shortEssayInterval);
    this.sendAnswer();
  }

  sendAnswer(sectionIndex = this.props.data.sectionIndex,
             questionIndex = this.props.data.questionIndex,
             data = this.props.data.questionData.q_clozes_answer_hidden,
             bufferData = this.state.bufferData,
             questionId = this.props.data.questionData.id) {
    const { updateShortEssayAnswer } = this.props;

    let tempAnswerArray = [];
    let tempUpdateArray = [];
    data.forEach((obj, index) => {
      tempAnswerArray.push({
        answer: bufferData[index],
        answerable_id: obj.id,
      })
      tempUpdateArray.push(bufferData[index])
    })

    updateShortEssayAnswer({
      // answerable_id: this.props.data.questionData.q_essay_possible_answers_without_answer.id,
      answerable_type: 'cloze',
      questionable_id: questionId,
      questionable_type: 'cloze',
      answer: tempAnswerArray,
      update: tempUpdateArray,
      questionIndex,
      sectionIndex,
    })
  }

  render() {
    const {
      data, data: { questionData }, prevQuestion, nextQuestion, updateAnswer, asyncAnswer,
    } = this.props;
    let { bufferData } = this.state;
    const { renderLabel, handleUpdateAnswer } = this;
    return (
      <Card className="card1">
        <Card.Body className="card-body1 d-flex">
          <div className="container-fluid d-flex flex-column">
            <div className="d-flex">
              <p>
                {data.questionIndex + 1}
                .
              </p>
              {/* <p>
                {this.state.sending.toString()}
              </p>
               <p>
                {questionData.question}
              </p> */}
              <div dangerouslySetInnerHTML={{ __html: questionData.q_clozes_group }} />
              {/*<MathJax math={questionData.q_clozes_group} />*/}
            </div>
            <div className="jawab my-2">
              <p>Jawab</p>
            </div>
            {
              // questionData.answers[0].field > 0 &&
              // renderLabel(questionData.answers[0].field).map((item) => (
              renderLabel(questionData.q_clozes_answer_hidden.length).map((item, index) => (
                <label className="label d-flex align-items-center">
                  <Row className="answer-row flex-nowrap">
                    <Col className="my-2" xs={1} md={1} lg={1}>
                      <p className="my-0 mx-2">
                        {String.fromCharCode(65 + item.value)}
                      </p>
                    </Col>
                    <Col xs={11} md={11} lg={11}>
                      <div>
                        {/* <p onInput={(e) => console.log(e.target.childNodes)} contenteditable="true" className="border-bottom-answer"> */}
                        <Form.Control
                          value={bufferData[index] ? bufferData[index] : ''}
                          onChange={(e) => {
                            // console.log('form onchange')
                            // let temp = data.answerData;
                            //
                            // temp[index] = e.target.value;
                            // console.log(e.target.value)
                            const temp = bufferData;
                            temp[index] = e.target.value;
                            //
                            this.setState({
                              bufferData: temp,
                            });

                            asyncAnswer({
                              answerable_id: questionData.q_clozes_answer_hidden[index].id,
                              answerable_type: 'cloze',
                              questionable_id: questionData.id,
                              questionable_type: 'cloze',
                              answer: e.target.value,
                              update: temp,
                            }, index);
                            // console.log(e.target.value);
                          }}
                          as="textarea"
                          rows="3"
                        />
                        {/* {data.answerData[index]} */}
                        {/* </p> */}
                      </div>
                    </Col>
                  </Row>
                </label>
              ))
            }
          </div>
        </Card.Body>
        <Card.Footer>
          <div className="bottom">
            <div className="section-name">
              Bag.
              {` ${data.sectionIndex + 1}`}
            </div>
            <div className="bottom-div">
              <Button className="btn btn-outline-secondary btn-light" onClick={prevQuestion}>
                <MdKeyboardArrowLeft className="icon" />
              </Button>
              <div className="bottom-div-div">
                {data.questionIndex + 1}
                /
                {data.questionLength}
              </div>
              <Button className="btn btn-outline-secondary btn-light" onClick={nextQuestion}>
                <MdKeyboardArrowRight className="icon" />
              </Button>
            </div>
          </div>
        </Card.Footer>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { answering } = state;
  return { answering };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleAnswering,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionCardShortEssay);
QuestionCardShortEssay.propTypes = {
  data: PropTypes.objectOf(PropTypes.object),
};

QuestionCardShortEssay.defaultProps = {
  data: null,
};
