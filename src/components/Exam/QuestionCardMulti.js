/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';
import './index.css';
import { Card, Image, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleAnswering } from '../../actions';

class QuestionCardMulti extends Component {
  constructor(props) {
    super(props);

    this.node = React.createRef()
    // console.log(props);
    this.state = {
      data: props.data,
    };
  }

  componentDidMount(){
    this.props.toggleAnswering({ answering: true })
    window.MathJax.Hub.Queue([
      "Typeset",
      window.MathJax.Hub,
      this.node.current
    ]);
  }

  static getDerivedStateFromProps(props, state) {
    // console.log(props);
    if (props.data !== state.data) {
      return {
        data: props.data,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    window.MathJax.Hub.Queue([
      "Typeset",
      window.MathJax.Hub,
      this.node.current
    ]);
  }

  render() {
    const {
      data: { questionData }, prevQuestion, nextQuestion, updateAnswer,
    } = this.props;
    const {
      data
    } = this.state;

    // console.log('data',this.props);

    return (
      <Card className="card1">
        <Card.Body className="card-body1">
          <div className="container-fluid d-flex flex-column">
            <div className="d-flex">
              <p className="mr-1">
                {data.questionIndex + 1}
                .
              </p>
              <div dangerouslySetInnerHTML={{__html: questionData.type === "mc" ? questionData.q_multiple_choice : questionData.q_true_false}} />
            </div>
            <div className="jawab my-2">
              <p>Jawab</p>
              {/* <p>{data.answerData}</p> */}
            </div>
            {/* {
              questionData.images.length > 0
              && (
                <div className="d-flex justify-content-start align-items-start flex-wrap">
                  { questionData.images.map((item) => <Image src={item} alt="empty" className="img" />) }
                </div>
              )
            } */}
            {
              questionData.type === "mc"
              && questionData.q_mc_possible_answers_without_answer.length > 0
              && questionData.q_mc_possible_answers_without_answer.map((item) => (
                <label className="label">
                  <div>
                    <input
                      checked={parseInt(data.answerData, 10) === item.id}
                      className="radio-button"
                      type="radio"
                      value={item.id}
                      onChange={() => {
                        updateAnswer({
                          answerable_id: item.id,
                          answerable_type: 'mc',
                          questionable_id: item.q_multiple_choice_id,
                          questionable_type: 'mc',
                          answer: null,
                          update: item.id,
                        });
                      }}
                      // onClick={() => {
                      //   if (data.answerData === item.id) {
                      //     updateAnswer(null);
                      //   }
                      // }}
                    />
                  </div>
                  <div dangerouslySetInnerHTML={{__html: item.answer}} />
                  {/* {item.answer} */}
                </label>
              ))
            }
            {
              questionData.type === "tf"
              && questionData.q_t_f_possible_answers_without_answer.length > 0
              && questionData.q_t_f_possible_answers_without_answer.map((item) => (
                <label className="label">
                  <div>
                    <input
                      checked={parseInt(data.answerData,10) === item.id}
                      className="radio-button"
                      type="radio"
                      value={item.id}
                      onChange={() => {
                        updateAnswer({
                          answerable_id: item.id,
                          answerable_type: 'tf',
                          questionable_id: item.q_true_false_id,
                          questionable_type: 'tf',
                          answer: null,
                          update: item.id,
                        });
                      }}
                      // onClick={() => {
                      //   if (data.answerData === item.id) {
                      //     updateAnswer(null);
                      //   }
                      // }}
                    />
                  </div>
                  {/* {String(item.answer).toUpperCase()} */}
                  <div dangerouslySetInnerHTML={{__html: item.answer}} />
                </label>
              ))
            }
          </div>
        </Card.Body>
        <Card.Footer>
          <div className="bottom">
            <div className="section-name">
              Bag.
              {` ${data.sectionIndex + 1}`}
            </div>
            <div className="bottom-div">
              <Button className="btn btn-outline-secondary btn-light" onClick={prevQuestion}>
                <MdKeyboardArrowLeft className="icon" />
              </Button>
              <div className="bottom-div-div">
                {data.questionIndex + 1}
                /
                {data.questionLength}
              </div>
              <Button className="btn btn-outline-secondary btn-light" onClick={nextQuestion}>
                <MdKeyboardArrowRight className="icon" />
              </Button>
            </div>
          </div>
        </Card.Footer>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { answering } = state;
  return { answering };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleAnswering,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionCardMulti);

QuestionCardMulti.propTypes = {
  data: PropTypes.objectOf(PropTypes.object),
};

QuestionCardMulti.defaultProps = {
  data: null,
};
