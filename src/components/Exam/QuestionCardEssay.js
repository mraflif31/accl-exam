/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';
import './index.css';
import { Card, Image, Button } from 'react-bootstrap';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from 'ckeditor5-build-math';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleAnswering } from '../../actions';
import MathJax from 'react-mathjax-preview';

class QuestionCardEssay extends Component {

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   // console.log('essaydidupdate');
  //   const { sending, changed, bufferRequest } = this.state;
  //   const { updateAnswer } = this.props;
  //   if (bufferRequest !== null && !sending) {
  //     const buff = bufferRequest;
  //     this.setState({
  //       bufferRequest: null,
  //       sending: true,
  //     });
  //     updateAnswer(buff);
  //   }
  // }

  constructor(props) {
    super(props);
    // console.log('initial props');
    // console.log(props);
    this.essayInterval = null;

    this.node = React.createRef()
    this.state = {
      data: props.data,
      sending: false,
      // changed: false,
      bufferData: props.data.answerData,
      bufferRequest: null,
      changeQuestion: false,
    };

    this.sendAnswer = this.sendAnswer.bind(this);
  }

  componentDidMount() {
    const { toggleAnswering, updateAnswer, data: { questionData } } = this.props;
    const { sendAnswer } = this;

    toggleAnswering({ answering: true });
    this.essayInterval = setInterval(() => {
      sendAnswer();
    }, 5000);
    window.MathJax.Hub.Queue([
      "Typeset",
      window.MathJax.Hub,
      this.node.current
    ]);
  }

  componentWillUnmount() {
    // console.log(this.essayInterval);
    // console.log('unmount success')
    clearInterval(this.essayInterval);
    this.sendAnswer();
  }

  static getDerivedStateFromProps(props, state) {
    if (props.data !== state.data) {
        // clearInterval(this.essayInterval);
        // this.sendAnswer();
        return {
          data: props.data,
        };
    }
    // return {
    //   data: props.data,
    // };
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.data.sectionIndex != this.props.data.sectionIndex
      || prevProps.data.questionIndex != this.props.data.questionIndex) {
      window.MathJax.Hub.Queue([
        "Typeset",
        window.MathJax.Hub,
        this.node.current
      ]);
      // console.log('updateee')
      // console.log(this.state)
      // console.log(this.props)
      this.setState({
        bufferData: this.props.data.answerData == null ? '' : this.props.data.answerData,
      })
      clearInterval(this.essayInterval);
      this.sendAnswer(prevProps.data.sectionIndex,
        prevProps.data.questionIndex,
        prevProps.data.questionData,
        prevState.bufferData);

      this.essayInterval = setInterval(() => {
        this.sendAnswer();
      }, 10000);
    }
  }

  sendAnswer(sectionIndex = this.props.data.sectionIndex,
             questionIndex = this.props.data.questionIndex,
             data = this.props.data.questionData,
             bufferData = this.state.bufferData) {
    const { updateAnswer } = this.props;
    updateAnswer({
      answerable_id: data.q_essay_possible_answers_without_answer.id,
      answerable_type: 'essay',
      questionable_id: data.id,
      questionable_type: 'essay',
      answer: bufferData ? bufferData.toString() : '',
      update: bufferData ? bufferData.toString() : '',
      sectionIndex,
      questionIndex,
    })
  }

  render() {
    const {
      data: { questionData }, prevQuestion, nextQuestion, updateAnswer, asyncAnswer,
    } = this.props;
    const {
      data, bufferData,
    } = this.state;
    return (
      <Card className="card1">
        <Card.Body>
          <div className="container-fluid d-flex flex-column">
            <div className="d-flex mb-2">
              <p>
                {data.questionIndex + 1}
                .
              </p>
              {/* <p>
                {questionData.question}
              </p> */}
              <div dangerouslySetInnerHTML={{__html: questionData.q_essay}} />
            </div>
            <div className="jawab my-2">
              <p>Jawab</p>
            </div>
            {/* {
              questionData.images.length > 0
              && (
                <div className="d-flex justify-content-start align-items-start flex-wrap">
                  { questionData.images.map((item) => <Image src={item} alt="empty" className="img" />) }
                </div>
              )
            } */}
            <CKEditor
              editor={ClassicEditor}
              data={bufferData ? bufferData : ''}
              // onInit={editor => {
              //   // You can store the "editor" and use when it is needed.
              //   console.log('Editor is ready to use!', editor);
              // }}
              onChange={(event, editor) => {
                // console.log("event")
                // console.log(event)
                const datas = editor.getData();
                // console.log({ event, editor, datas });a
                // console.log(bufferData)
                this.setState({
                  bufferData: datas,
                })
                asyncAnswer({
                  answerable_id: questionData.q_essay_possible_answers_without_answer.id,
                  answerable_type: 'essay',
                  questionable_id: questionData.id,
                  questionable_type: 'essay',
                  answer: datas,
                  update: datas,
                });
              }}
              // onBlur={(event, editor) => {
              //   this.props.toggleAnswering({ answering: true });
              // }}
              onFocus={(event, editor) => {
                this.props.toggleAnswering({ answering: true });
              }}
            />
          </div>
        </Card.Body>
        <Card.Footer>
          <div className="bottom">
            <div className="section-name">
              Bag.
              {` ${data.sectionIndex + 1}`}
            </div>
            <div className="bottom-div">
              <Button className="btn btn-outline-secondary btn-light" onClick={prevQuestion}>
                <MdKeyboardArrowLeft className="icon" />
              </Button>
              <div className="bottom-div-div">
                {data.questionIndex + 1}
                /
                {data.questionLength}
              </div>
              <Button className="btn btn-outline-secondary btn-light" onClick={nextQuestion}>
                <MdKeyboardArrowRight className="icon" />
              </Button>
            </div>
          </div>
        </Card.Footer>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { answering } = state;
  return { answering };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleAnswering,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionCardEssay);


QuestionCardEssay.propTypes = {
  data: PropTypes.objectOf(PropTypes.object),
};

QuestionCardEssay.defaultProps = {
  data: null,
};
