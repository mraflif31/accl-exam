/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';
import './index.css';
import {
  // eslint-disable-next-line no-unused-vars
  Card, Row, Col, Dropdown, DropdownButton, Table, Button, Form,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleAnswering } from '../../actions';
import MathJax from 'react-mathjax-preview';

class QuestionCardMatch extends Component {
  constructor(props) {
    super(props);

    this.node = React.createRef()
    // console.log(props);
  }

  componentDidMount(){
    this.props.toggleAnswering({ answering: true })
    // console.log('state',this.state);
    window.MathJax.Hub.Queue([
      "Typeset",
      window.MathJax.Hub,
      this.node.current
    ]);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    window.MathJax.Hub.Queue([
      "Typeset",
      window.MathJax.Hub,
      this.node.current
    ]);
  }

  // static getDerivedStateFromProps(props, state) {
  //   // console.log(props);
  //   window.MathJax.Hub.Queue([
  //     "Typeset",
  //     window.MathJax.Hub,
  //     this.node.current
  //   ]);
  // }

  renderAnswers(answers) {
    let arr = [];
    arr.push(answers.q_matchings);
    arr.push(answers.q_matching_possible_answers);

    let temp = [];
    for (let i = 0; i < arr[1].length; i += 1) {
      temp.push({
        question: arr[0][i] ? arr[0][i].q_matching : null,
        qmatch_id: arr[0][i] ? arr[0][i].id : null,
        answer: arr[1][i].q_matching_possible_answer,
        value: i + 1,
        id: `drop${i}`,
      });
    }
    return temp;
  }

  render() {
    const {
      data, data: { questionData }, prevQuestion, nextQuestion, updateAnswer,
    } = this.props;
    const { renderAnswers } = this;

    return (
      <Card className="card1">
        <Card.Body className="card-body1 d-flex">
          <div className="card-text1 container-fluid justify-content-center">
            <div className="d-flex">
              <p>
                {data.questionIndex + 1}
                .
              </p>
              {/* <p>
                {questionData.question}
              </p> */}
              <div dangerouslySetInnerHTML={{__html: questionData.q_matching_group}} />
              {/*<MathJax math={questionData.q_matching_group} />*/}
            </div>
            <Table responsive="md" borderless>
              <tbody>
                {
                  questionData.q_matchings.length !== 0
                  && renderAnswers(questionData).map((item, index) => {
                    if (item.question !== null) {
                      return (
                        <tr>
                          <td className="wrap-question-answer">
                            {item.value}
                            .
                            {' '}
                            {item.question}
                          </td>
                          <td className="wrap-choose-answer">
                            <Form.Control
                              onChange={(e) => {
                                let temp = data.answerData;
                                // console.log('temp', temp, typeof temp);
                                temp[index] = e.target.value;
                                updateAnswer({
                                  answerable_id: e.target.value,
                                  answerable_type: 'matching',
                                  questionable_id: item.qmatch_id,
                                  questionable_type: 'matching',
                                  answer: null,
                                  update: temp,
                                });
                                // console.log(e.target.value);
                              }}
                              value={data.answerData[index]}
                              as="select"
                            >
                              <option value={-1} disabled>Select</option>
                              {
                                questionData.q_matching_possible_answers.map((obj, i) => (
                                  // <Dropdown.Item eventKey={obj.id} onSelect={() => console.log("droponchange")}>{String.fromCharCode(96 + obj.id)}</Dropdown.Item>
                                  <option value={obj.id}>{String.fromCharCode(97 + i)}</option>
                                ))
                              }
                            </Form.Control>
                          </td>
                          <td className="wrap-question-answer border-left">
                            {String.fromCharCode(96 + item.value)}
                            .
                            {' '}
                            {item.answer}
                          </td>
                        </tr>
                      );
                    }
                    return (
                      <tr>
                        <td className="wrap-question-answer" />
                        <td className="wrap-choose-answer" />
                        <td className="wrap-question-answer border-left">
                          {String.fromCharCode(96 + item.value)}
                          .
                          {' '}
                          {item.answer}
                        </td>
                      </tr>
                    );
                  })
                }
              </tbody>
            </Table>
          </div>
        </Card.Body>
        <Card.Footer>
          <div className="bottom">
            <div className="section-name">
              Bag.
              {` ${data.sectionIndex + 1}`}
            </div>
            <div className="bottom-div">
              <Button className="btn btn-outline-secondary btn-light" onClick={prevQuestion}>
                <MdKeyboardArrowLeft className="icon" />
              </Button>
              <div className="bottom-div-div">
                {data.questionIndex + 1}
                /
                {data.questionLength}
              </div>
              <Button className="btn btn-outline-secondary btn-light" onClick={nextQuestion}>
                <MdKeyboardArrowRight className="icon" />
              </Button>
            </div>
          </div>
        </Card.Footer>
      </Card>
    );
  }
}


const mapStateToProps = (state) => {
  const { answering } = state;
  return { answering };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toggleAnswering,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionCardMatch);


QuestionCardMatch.propTypes = {
  data: PropTypes.objectOf(PropTypes.object),
};

QuestionCardMatch.defaultProps = {
  data: null,
};
