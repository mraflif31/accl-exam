/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Col, Row, Button, Card, Modal, Container,
} from 'react-bootstrap';
import axios from 'axios';
import { AsyncStorage } from 'AsyncStorage';
import { Redirect } from 'react-router-dom';

import './index.css';
import moment from 'moment';
import QuestionCardMulti from './QuestionCardMulti';
import QuestionCardEssay from './QuestionCardEssay';
import QuestionCardMatch from './QuestionCardMatch';
import QuestionCardShortEssay from './QuestionCardShortEssay';
import { toQuestion } from '../../actions';
import TimerCard from './TimerCard';
import InfoCard from './InfoCard';
import 'moment/locale/id';
import { API_URL } from '../../constants/api-domain';

class QuestionCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      show1: false,
      questionIndex: props.questionIndex,
      sectionIndex: props.sectionIndex,
      questions: props.questions,
      questionVisited: props.questionVisited,
      answers: props.answers,
      redirect: false,
      time: new Date(props.time),
    };

    // console.log(this.props);

    this.processNextQuestion = this.processNextQuestion.bind(this);
    this.processPrevQuestion = this.processPrevQuestion.bind(this);
    this.changeVisitedArray = this.changeVisitedArray.bind(this);
    this.renderNumbers = this.renderNumbers.bind(this);
    this.renderNumberCard = this.renderNumberCard.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleFinish = this.handleFinish.bind(this);
    this.handleConfirmation = this.handleConfirmation.bind(this);
    this.updateAnswer = this.updateAnswer.bind(this);
    this.updateShortEssayAnswer = this.updateShortEssayAnswer.bind(this);
    this.asyncAnswer = this.asyncAnswer.bind(this);
    this.buttonType = this.buttonType.bind(this);
    this.countTotalQuestion = this.countTotalQuestion.bind(this);
    this.countUndoneQuestion = this.countUndoneQuestion.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.questionIndex !== state.questionIndex
      || props.sectionIndex !== state.sectionIndex
      || props.questionVisited !== state.questionVisited
      || props.answers !== state.answers) {
      return {
        sectionIndex: props.sectionIndex,
        questionIndex: props.questionIndex,
        questionVisited: props.questionVisited,
        answers: props.answers,
      };
    }
    return null;
  }

  async componentDidMount() {
    moment.locale('id');
    const prof = await AsyncStorage.getItem('profile');

    if (prof !== null) {
      const data = JSON.parse(prof);
      const finishTime = new Date(new Date(data.exam.date).getTime() + data.exam.duration*60000);
      // console.log(finishTime);
      const remainingTime = (finishTime - this.state.time) / 1000;
      this.setState({
        sessionToken: data.session_token,
        hours: Math.floor(remainingTime / 3600),
        minutes: Math.floor(remainingTime % 3600 / 60),
        seconds: remainingTime % 3600 % 60,
        userId: data.user_id,
        examId: data.exam.id,
      });
      axios.post(`${API_URL}/api/exam/card`, {
        user_id: this.state.userId,
        exam_id: this.state.examId,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              exam_type: res.data.exam.exam_type,
              exam_date: res.data.exam.date,
              subject: res.data.exam.subject,
              subject_image: res.data.exam.subject_image,
              school_year: res.data.exam.school_year,
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    setInterval(() => this.startCountdown(),1000);
  }

  startCountdown(){
    let {hours, minutes, seconds} = this.state;
    if (hours <= 0 && minutes <= 0 && seconds <= 0){
      this.handleFinish();
      // console.log("DONE BRUV");
    }else{
      if (seconds > 0){
        this.setState({seconds: seconds - 1});
      }else{
        if (minutes > 0){
          this.setState({minutes: minutes - 1});
          this.setState({seconds: 59});
        }else{
          if (hours > 0){
            this.setState({
              hours: hours - 1,
              minutes: 59,
              seconds: 59,
            });
          }
        }
      }
    }
  }

  handleShow(state) {
    this.setState({ [state]: true });
  }

  handleClose(state) {
    this.setState({ [state]: false });
  }

  handleConfirmation() {
    this.setState({ show: false });
    this.setState({ show1: true });
  }

  handleFinish() {
    axios.post(`${API_URL}/api/exam/update-finish`, {
      token: this.props.token,
    })
      .then(async (res) => {
        if (res.status === 200) {
          clearInterval(this.countdown);
          await AsyncStorage.setItem('profile', null);
          await AsyncStorage.setItem('questions', null);
          await AsyncStorage.setItem('questionVisited', null);
          await AsyncStorage.setItem('answers', null);
          await AsyncStorage.setItem('examId', null);
          await AsyncStorage.setItem('sectionIndex', null);
          await AsyncStorage.setItem('questionIndex', null);
          this.setState({ redirect: true });
        }
        if (res.status === 202) {
          console.log('Update Data Finish Failed');
        }
      });
  }

  changeVisitedArray(sectionIndex, questionIndex) {
    const { questionVisited } = this.state;
    let arr = questionVisited;

    if (questionVisited[sectionIndex][questionIndex].visited === false) {
      arr = questionVisited;
      arr[sectionIndex][questionIndex].visited = true;
    }

    return arr;
  }

  pad(n, size = 0){
    var s = String(n);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }

  processNextQuestion() {
    const { questionIndex, sectionIndex, questions } = this.state;
    const { toQuestion } = this.props;
    const { changeVisitedArray, handleAsyncAnswer } = this;

    if (questionIndex < questions[sectionIndex].length - 1) {
      const tempChangeVisitedArray = changeVisitedArray(sectionIndex, questionIndex + 1);
      const tempQuestionIndex = questionIndex + 1;
      toQuestion({
        questionIndex: tempQuestionIndex,
        questionVisited: tempChangeVisitedArray,
      });
      handleAsyncAnswer('questionIndex', tempQuestionIndex.toString());
      handleAsyncAnswer('questionVisited', JSON.stringify(tempChangeVisitedArray));
    } else if (sectionIndex < questions.length - 1) {
      const tempChangeVisitedArray = changeVisitedArray(sectionIndex + 1, 0);
      const tempQuestionIndex = 0;
      const tempSectionIndex = sectionIndex + 1;
      toQuestion({
        questionIndex: 0,
        sectionIndex: sectionIndex + 1,
        questionVisited: changeVisitedArray(sectionIndex + 1, 0),
      });
      handleAsyncAnswer('questionIndex', tempQuestionIndex.toString());
      handleAsyncAnswer('sectionIndex', tempSectionIndex.toString());
      handleAsyncAnswer('questionVisited', JSON.stringify(tempChangeVisitedArray));
    }
    // console.log(this.props)
  }

  processPrevQuestion() {
    const { questionIndex, sectionIndex, questions } = this.state;
    const { toQuestion } = this.props;
    const { changeVisitedArray, handleAsyncAnswer } = this;

    if (questionIndex > 0) {
      const tempChangeVisitedArray = changeVisitedArray(sectionIndex, questionIndex - 1);
      const tempQuestionIndex = questionIndex - 1;
      toQuestion({
        questionIndex: tempQuestionIndex,
        questionVisited: tempChangeVisitedArray,
      });
      handleAsyncAnswer('questionIndex', tempQuestionIndex.toString());
      handleAsyncAnswer('questionVisited', JSON.stringify(tempChangeVisitedArray));
    } else if (sectionIndex > 0) {
      const tempChangeVisitedArray = changeVisitedArray(sectionIndex - 1, questions[sectionIndex - 1].length - 1);
      const tempQuestionIndex = questions[sectionIndex - 1].length - 1;
      const tempSectionIndex = sectionIndex - 1;
      toQuestion({
        questionIndex: tempQuestionIndex,
        sectionIndex: tempSectionIndex,
        questionVisited: tempChangeVisitedArray,
      });
      handleAsyncAnswer('questionIndex', tempQuestionIndex.toString());
      handleAsyncAnswer('sectionIndex', tempSectionIndex.toString());
      handleAsyncAnswer('questionVisited', JSON.stringify(tempChangeVisitedArray));
    }
    // console.log(this.props)
  }

  async handleAsyncAnswer(key, obj) {
    // console.log('handle asycn')
    // console.log(key)
    // console.log(obj)
    await AsyncStorage.setItem(key, obj);
  }

  asyncAnswer(answer) {
    const {
      questionIndex, sectionIndex, answers,
    } = this.state;

    const temp = answers;
    temp[sectionIndex][questionIndex] = answer.update;

    toQuestion({
      answers: temp,
    });
    this.forceUpdate();
    this.handleAsyncAnswer('answers', JSON.stringify(temp));
  }

  updateShortEssayAnswer(answer) {
    const {
      questionIndex, sectionIndex, answers, sessionToken, userId,
    } = this.state;
    const { toQuestion } = this.props;

    // console.log('answer.answer')
    // console.log(answer.answer)

    let tempAnswersArray = []
    answer.answer.forEach((obj) => tempAnswersArray.push(obj));

    axios.post(`${API_URL}/api/exam/answer-short-essay`, {
      user_id: userId,
      // answerable_id: answer.answerable_id,
      answerable_type: answer.answerable_type,
      questionable_id: answer.questionable_id,
      questionable_type: answer.questionable_type,
      answer: tempAnswersArray,
      token: sessionToken,
    })
      .then((res) => {

        let temp = answers;

        answer.answer.forEach((obj, index) => {
          temp[answer.sectionIndex][answer.questionIndex][index] = obj.answer;
        })
        // console.log('shortessay temp answer')
        // console.log(temp)

        // toQuestion({
        //   answers: temp,
        // });
        // this.forceUpdate();
        this.handleAsyncAnswer('answers', JSON.stringify(temp));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  updateAnswer(answer) {
    const {
      questionIndex, sectionIndex, answers, sessionToken, userId,
    } = this.state;
    const { toQuestion } = this.props;

    axios.post(`${API_URL}/api/exam/answer`, {
      user_id: userId,
      answerable_id: answer.answerable_id,
      answerable_type: answer.answerable_type,
      questionable_id: answer.questionable_id,
      questionable_type: answer.questionable_type,
      answer: answer.answer,
      token: sessionToken,
    })
      .then((res) => {
        const temp = answers;

        if (answer.sectionIndex != null && answer.questionIndex != null) {
          temp[answer.sectionIndex][answer.questionIndex] = answer.update;
        } else {
          temp[sectionIndex][questionIndex] = answer.update;
        }
        // console.log('answers')
        // console.log(answers)
        // console.log(JSON.stringify(answer, null, 4))
        // console.log(JSON.stringify(temp, null, 4))

        toQuestion({
          answers: temp,
        });
        this.forceUpdate();
        this.handleAsyncAnswer('answers', JSON.stringify(temp));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  buttonType(i, j) {
    const { questionVisited } = this.props;
    const { questions, answers } = this.state;

    if (questions[i][j].type === 'matching' || questions[i][j].type === 'cloze') {
      if (answers[i][j].length > 0) {
        for (let k = 0; k < answers[i][j].length; k += 1) {
          if (answers[i][j][k] === null || answers[i][j][k] === -1 || answers[i][j][k] === '' || answers[i][j][k] === undefined) {
            if (questionVisited[i][j].visited !== true) {
              return 'outline-secondary';
            }
            return 'secondary';
          }
        }
        return 'success';
      }
    } else {
      if (answers[i][j] !== null && answers[i][j] !== '') {
        return 'success';
      }
    }
    if (questionVisited[i][j].visited !== true) {
      return 'outline-secondary';
    }
    return 'secondary';
  }

  countTotalQuestion() {
    const { questions } = this.state;
    let temp = 0;

    for (let i = 0; i < questions.length; i += 1) {
      temp += questions[i].length;
    }

    return temp;
  }

  countUndoneQuestion() {
    const { answers, questions } = this.state;
    let temp = 0;

    for (let i = 0; i < answers.length; i += 1) {
      for (let j = 0; j < answers[i].length; j += 1) {
        if (questions[i][j].type === 'matching' || questions[i][j].type === 'cloze') {
          for (let k = 0; k < answers[i][j].length; k += 1) {
            if (answers[i][j][k] === null || answers[i][j][k] === -1 || answers[i][j][k] === '') {
              temp += 1;
              break;
            }
          }
        } else if (answers[i][j] === null || answers[i][j] === '') {
          temp += 1;
        }
        // return 'success';
      }
    }

    return temp;
  }

  renderNumberCard() {
    const { renderNumbers, countTotalQuestion, countUndoneQuestion } = this;
    const total = countTotalQuestion();
    const undone = countUndoneQuestion();
    const done = total - undone;
    return (
      <Card className="card4">
        <Card.Body className="card-body4">
          <div className="card-text4">
            {renderNumbers()}
          </div>
        </Card.Body>
        <Button className="selesai-button" onClick={() => this.handleShow('show')}>
          SELESAI
        </Button>
        <Modal show={this.state.show} centered>
          <Modal.Body>
            <Container>
              <Row className="align-items-center">
                <Col xs={4} md={4} className="text-right pr-0 text-black-50">
                  Soal Diisi:
                </Col>
                <Col className="text-warning text-left pr-0" xs={2} md={2}>
                  <p style={{ fontSize: 25, color: 'limegreen' }} className="mb-0">
                    {done}
                  </p>
                </Col>
                <Col xs={4} md={4} className="text-right px-0 text-black-50">
                  Soal Kosong:
                </Col>
                <Col className="text-secondary text-left pr-0" xs={2} md={2}>
                  <p style={{ fontSize: 25, color: 'slategray' }} className="mb-0">
                    {undone}
                  </p>
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={12} className="text-center" style={{ fontSize: 25 }}>
                  <p style={{ color: 'lightslategrey', fontFamily: 'Monospace' }} className="my-1 text-secondary">{this.pad(this.state.hours)} : {this.pad(this.state.minutes)} : {this.pad(this.state.seconds)}</p>
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={12} className="text-center mt-3">
                  <p className="font-italic" style={{ fontSize: 12 }}>
                    <b>Yakin sudah selesai mengerjakan Ujian?</b>
                  </p>
                </Col>
              </Row>
              <Row>
                <Col xs={6} md={6} className="text-right">
                  <Button variant="outline-primary" onClick={() => this.handleConfirmation()}>
                    Ya
                  </Button>
                </Col>
                <Col xs={6} md={6} className="text-left">
                  <Button variant="outline-secondary" onClick={() => this.handleClose('show')}>
                    Tidak
                  </Button>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
        <Modal show={this.state.show1} centered>
          <Modal.Body>
            <Container>
              <Row>
                <Col className="text-center mt-2">
                  <p className="mb-0">Selamat! Kamu sudah menyelesaikan ujian. Untuk melihat hasil ujian, silakan klik link berikut</p>
                </Col>
              </Row>
              <Row>
                <Col className="text-center">
                  <a className="btn text-info" href="http://facebook.com">bit.ly/bitbitlyly</a>
                </Col>
              </Row>
              <Row>
                <Col className="text-center mt-5">
                  <Button variant="outline-secondary" onClick={() => this.handleFinish()}>Close</Button>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
      </Card>
    );
  }

  renderNumbers() {
    // const { questionVisited } = this.state;
    const { toQuestion, questionVisited } = this.props;
    const { changeVisitedArray, buttonType, handleAsyncAnswer } = this;
    const arr = [];

    for (const [i, item] of questionVisited.entries()) {
      const childButton = [];
      for (const [j, obj] of item.entries()) {
        childButton.push(
          <Button
            key={i.toString() + j.toString()}
            className="exam-number-button"
            // variant={questionVisited[i][j].visited ? 'success' : 'outline-secondary'}
            variant={buttonType(i, j)}
            onClick={() => {
              const tempChangeVisitedArray = changeVisitedArray(i, j);
              const tempQuestionIndex = j;
              const tempSectionIndex = i;
              toQuestion({
                sectionIndex: tempSectionIndex,
                questionIndex: tempQuestionIndex,
                questionVisited: changeVisitedArray(i, j),
              })
              handleAsyncAnswer('questionIndex', tempQuestionIndex.toString());
              handleAsyncAnswer('sectionIndex', tempSectionIndex.toString());
              handleAsyncAnswer('questionVisited', JSON.stringify(tempChangeVisitedArray));}
            }
          >
            {j + 1}
          </Button>,
        );
      }
      arr.push(
        <div className="mb-0" key={i.toString()}>
          <p className="ml-2 mb-1 text-muted">
            Bag
            {' '}
            {i + 1}
          </p>
          {/* <div className="exam-button-div"> */}
          <div
            className="exam-button-div2"
          >
            {childButton}
          </div>
          {/* </div> */}
        </div>,
      );
    }
    return arr;
  }

  render() {
    if (this.state.redirect) {
      return (<Redirect exact to="/" />);
    }
    const {
      questions, sectionIndex, questionIndex, answers,
    } = this.state;
    const {
      processNextQuestion, processPrevQuestion, renderNumberCard, updateAnswer, asyncAnswer, updateShortEssayAnswer
    } = this;
    // const { questionVisited } = this.props;
    const data = {
      questionIndex,
      sectionIndex,
      questionData: questions[sectionIndex][questionIndex],
      questionLength: questions[sectionIndex].length,
      sectionLength: questions.length,
      answerData: answers[sectionIndex][questionIndex],
    };

    // console.log('data',data);
    // const {} = this.state;
    // console.log('answers');
    // console.log(answers);
    // console.log([sectionIndex, questionIndex]);

    switch (questions[sectionIndex][questionIndex].type) {
      case 'mc':
        return (
          <Row className="row">
            <Col xs={12} md={9} className="mt-2 d-flex">
              <QuestionCardMulti
                data={data}
                nextQuestion={processNextQuestion}
                prevQuestion={processPrevQuestion}
                updateAnswer={updateAnswer}
              />
            </Col>
            <Col xs={12} md={3} className="d-inline-flex flex-column py-0">
              <TimerCard
                hours={this.pad(this.state.hours)}
                minutes={this.pad(this.state.minutes)}
                seconds={this.pad(this.state.seconds)}
              />
              <InfoCard
                src={require('../../assets/images/empty_matpel.png')}
                top={`${this.state.exam_type} (${this.state.school_year})`}
                middle={this.state.subject}
                bottom={moment(this.state.exam_date).format('dddd, Do MMMM YYYY')}
              />
              {renderNumberCard()}
            </Col>
          </Row>
        );
      case 'tf':
        return (
          <Row className="row">
            <Col xs={12} md={9} className="mt-2 d-flex">
              <QuestionCardMulti
                data={data}
                nextQuestion={processNextQuestion}
                prevQuestion={processPrevQuestion}
                updateAnswer={updateAnswer}
              />
            </Col>
            <Col xs={12} md={3} className="d-inline-flex flex-column py-0">
              <TimerCard
                hours={this.pad(this.state.hours)}
                minutes={this.pad(this.state.minutes)}
                seconds={this.pad(this.state.seconds)}
              />
              <InfoCard
                src={require('../../assets/images/empty_matpel.png')}
                top={`${this.state.exam_type} (${this.state.school_year})`}
                middle={this.state.subject}
                bottom={moment(this.state.exam_date).format('dddd, Do MMMM YYYY')}
              />
              {renderNumberCard()}
            </Col>
          </Row>
        );
      case 'essay':
        return (
          <Row className="row">
            <Col xs={12} md={9} className="mt-2 d-flex">
              <QuestionCardEssay
                data={data}
                nextQuestion={processNextQuestion}
                prevQuestion={processPrevQuestion}
                updateAnswer={updateAnswer}
                asyncAnswer={asyncAnswer}
              />
            </Col>
            <Col xs={12} md={3} className="d-inline-flex flex-column py-0">
              <TimerCard
                hours={this.pad(this.state.hours)}
                minutes={this.pad(this.state.minutes)}
                seconds={this.pad(this.state.seconds)}
              />
              <InfoCard
                src={require('../../assets/images/empty_matpel.png')}
                top={`${this.state.exam_type} (${this.state.school_year})`}
                middle={this.state.subject}
                bottom={moment(this.state.exam_date).format('dddd, Do MMMM YYYY')}
              />
              {renderNumberCard()}
            </Col>
          </Row>
        );
      case 'cloze':
        return (
          <Row className="row">
            <Col xs={12} md={9} className="mt-2 d-flex">
              <QuestionCardShortEssay
                data={data}
                nextQuestion={processNextQuestion}
                prevQuestion={processPrevQuestion}
                updateAnswer={updateAnswer}
                asyncAnswer={asyncAnswer}
                updateShortEssayAnswer={updateShortEssayAnswer}
              />
            </Col>
            <Col xs={12} md={3} className="d-inline-flex flex-column py-0">
              <TimerCard
                hours={this.pad(this.state.hours)}
                minutes={this.pad(this.state.minutes)}
                seconds={this.pad(this.state.seconds)}
              />
              <InfoCard
                src={require('../../assets/images/empty_matpel.png')}
                top={`${this.state.exam_type} (${this.state.school_year})`}
                middle={this.state.subject}
                bottom={moment(this.state.exam_date).format('dddd, Do MMMM YYYY')}
              />
              {renderNumberCard()}
            </Col>
          </Row>
        );
      case 'matching':
        return (
          <Row className="row">
            <Col xs={12} md={9} className="mt-2 d-flex">
              <QuestionCardMatch
                data={data}
                nextQuestion={processNextQuestion}
                prevQuestion={processPrevQuestion}
                updateAnswer={updateAnswer}
              />
            </Col>
            <Col xs={12} md={3} className="d-inline-flex flex-column py-0">
              <TimerCard
                hours={this.pad(this.state.hours)}
                minutes={this.pad(this.state.minutes)}
                seconds={this.pad(this.state.seconds)}
              />
              <InfoCard
                src={require('../../assets/images/empty_matpel.png')}
                top={`${this.state.exam_type} (${this.state.school_year})`}
                middle={this.state.subject}
                bottom={moment(this.state.exam_date).format('dddd, Do MMMM YYYY')}
              />
              {renderNumberCard()}
            </Col>
          </Row>
        );
      default:
        return <p>Error</p>;
    }
  }
}

const mapStateToProps = (state) => {
  const { questions, answers, profile } = state;
  return { questions, answers, profile };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    toQuestion,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionCard);
