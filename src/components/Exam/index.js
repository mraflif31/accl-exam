import React, { Component } from 'react';
import './index.css';
import { Container, Spinner } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';

import { AsyncStorage } from 'AsyncStorage';
import QuestionCard from './QuestionCard';
import { addQuestion, createQuestionVisited, toggleAnswering } from '../../actions';
import { notifyOverwatch } from '../../utils';
import { NEW_TAB_OPENED } from '../../constants/action-types';
import { API_URL } from '../../constants/api-domain';

class Exam extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // questionIndex: 0,
      // sectionIndex: 0,
      loading: true,
      questionVisited: [],
    };
    this.onBlur = this.onBlur.bind(this);

    this.createQuestionsArray = this.createQuestionsArray.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    // console.log(props);
    if (props.questionIndex !== state.questionIndex
      || props.sectionIndex !== state.sectionIndex
      || props.questionVisited !== state.questionVisited) {
      return {
        sectionIndex: props.sectionIndex,
        questionIndex: props.questionIndex,
        questionVisited: props.questionVisited,
      };
    }
    return null;
  }

  async componentDidMount() {
    window.addEventListener('blur', this.onBlur);
    window.addEventListener('keydown', this.onInspectElement);
    window.addEventListener('contextmenu', this.onContextMenu);
    const { addQuestion } = this.props;
    const {
      createVisitedArray, createQuestionArray, createQuestionsArray, searchQuestionIndex,
    } = this;
    await AsyncStorage.getItem('profile')
      .then(async (value) => {
        if (value == null) {
          console.log('Data tidak ditemukan');
        } else {
          const data = JSON.parse(value);
          this.setState({ token: data.session_token, user_id: data.user_id });

          // console.log("data")
          // console.log(data)
          const asyncQuestions = await AsyncStorage.getItem('questions');
          const asyncQuestionVisited = await AsyncStorage.getItem('questionVisited');
          const asyncAnswers = await AsyncStorage.getItem('answers');
          const asyncExamId = await AsyncStorage.getItem('examId');

          // console.log('asyncAnswer', asyncAnswers);

          if (asyncQuestions && asyncQuestions != 'null' && asyncQuestionVisited && asyncQuestionVisited != 'null'
            && asyncAnswers && asyncAnswers != 'null' && asyncExamId && asyncExamId != 'null'
            && parseInt(asyncExamId, 10) === parseInt(data.exam.id, 10)) {
            // console.log('asynncccc')
            const asyncSectionIndex = await AsyncStorage.getItem('sectionIndex');
            const asyncQuestionIndex = await AsyncStorage.getItem('questionIndex');
            // console.log(asyncSectionIndex)
            // console.log(asyncQuestionIndex)


            axios.get(`${API_URL}/api/exam/get-time`).then(async (res) => {
              if (asyncSectionIndex && asyncSectionIndex != 'null' && asyncQuestionIndex && asyncQuestionIndex != 'null') {
                addQuestion({
                  questions: JSON.parse(asyncQuestions),
                  questionVisited: JSON.parse(asyncQuestionVisited),
                  answers: JSON.parse(asyncAnswers),
                  sectionIndex: parseInt(asyncSectionIndex, 10),
                  questionIndex: parseInt(asyncQuestionIndex, 10),
                });
              } else {
                addQuestion({
                  questions: JSON.parse(asyncQuestions),
                  questionVisited: JSON.parse(asyncQuestionVisited),
                  answers: JSON.parse(asyncAnswers),
                });
              }
              this.setState({
                loading: false,
                questionVisited: JSON.parse(asyncQuestionVisited),
                time: res.data.time.date,
              });
            });
          } else {
            axios.post(`${API_URL}/api/exam/get-questions`, {
              exam_code: data.exam.code,
              token: data.session_token,
              student_id: data.user_id,
            })
              .then(async (res) => {
                // console.log(res.data);
                // console.log(res.data);
                // let temp = createQuestionArray(res.data.data[0].sections[0]);
                const questionArray = createQuestionsArray(res.data.exam[0].sections);
                const visitedAnswerArray = createVisitedArray(questionArray);
                // console.log('visitedAnswerArray', visitedAnswerArray[1]);
                visitedAnswerArray[0][0][0].visited = true;
                if (res.data.student_exam.questionable_id !== 1 && res.data.student_exam.questionable_type !== 'default') {
                  const questSectionIndex = searchQuestionIndex(res.data.student_exam.questionable_id,
                    res.data.student_exam.questionable_type,
                    questionArray);
                  if (questSectionIndex !== null) {
                    addQuestion({
                      questions: questionArray,
                      questionVisited: visitedAnswerArray[0],
                      answers: visitedAnswerArray[1],
                      sectionIndex: questSectionIndex[0],
                      questionIndex: questSectionIndex[1],
                    });
                  } else {
                    addQuestion({
                      questions: questionArray,
                      questionVisited: visitedAnswerArray[0],
                      answers: visitedAnswerArray[1],
                    });
                  }
                } else {
                  addQuestion({
                    questions: questionArray,
                    questionVisited: visitedAnswerArray[0],
                    answers: visitedAnswerArray[1],
                  });
                }
                await AsyncStorage.setItem('questions', JSON.stringify(questionArray));
                await AsyncStorage.setItem('questionVisited', JSON.stringify(visitedAnswerArray[0]));
                await AsyncStorage.setItem('answers', JSON.stringify(visitedAnswerArray[1]));
                await AsyncStorage.setItem('examId', data.exam.id);

                this.setState({
                  loading: false,
                  questionVisited: visitedAnswerArray[0],
                  time: res.data.time.date,
                });
              })
              .catch((error) => {
                console.log(error);
              });
          }
        }
      });
  }

  onContextMenu(e) {
    e.preventDefault();
    return false;
  }

  onInspectElement(event) {
    if ((event.ctrlKey && event.shiftKey && event.keyCode === 73) || event.keyCode === 123) {
      event.preventDefault();
    }
  }

  // onBlur() {
  //   console.log('Not Focused');
  //   // alert("New tab opened");
  // };

  onBlur() {
    // console.log("blur")
    if (!(this.props.answering)) {
      notifyOverwatch(NEW_TAB_OPENED, this.state.user_id);
    } else {
      this.props.toggleAnswering({ answering: false });
    }
  }

  componentWilUnmount() {
    window.removeEventListener('blur', this.onBlur);
  }

  searchQuestionIndex(id, type, questionArray) {
    for (let i = 0; i < questionArray.length; i += 1) {
      for (let j = 0; j < questionArray[i].length; j += 1) {
        if (type == questionArray[i][j].type) {
          if (type === 'matching') {
            for (let k = 0; k < questionArray[i][j].q_matchings.length; k += 1) {
              if (questionArray[i][j].q_matchings[k].id == id) {
                return [i, j];
              }
            }
          } else if (questionArray[i][j].id == id) {
            return [i, j];
          }
        }
      }
    }
    return null;
  }

  createQuestionArray(question) {
    const questionArray = [];

    for (let i = 0; i < question.q_multiple_choices.length; i += 1) {
      questionArray.push(question.q_multiple_choices[i]);
    }

    for (let i = 0; i < question.q_essay.length; i += 1) {
      questionArray.push(question.q_essay[i]);
    }

    for (let i = 0; i < question.q_true_false.length; i += 1) {
      questionArray.push(question.q_true_false[i]);
    }

    for (let i = 0; i < question.q_matching_groups.length; i += 1) {
      questionArray.push(question.q_matching_groups[i]);
    }

    for (let i = 0; i < question.q_clozes_groups.length; i += 1) {
      questionArray.push(question.q_clozes_groups[i]);
    }

    return questionArray.sort((a, b) => parseInt(a.id, 10) - parseInt(b.id, 10));
  }

  createQuestionsArray(questions) {
    const { createQuestionArray } = this;
    const sectionArray = [];

    for (let i = 0; i < questions.length; i += 1) {
      sectionArray.push(createQuestionArray(questions[i]));
    }

    return sectionArray;
  }

  createVisitedArray(sections) {
    // let arrays = []

    const visitedSectionArray = [];
    const answerSectionArray = [];

    sections.forEach((questions) => {
      const visitedArray = [];
      const answerArray = [];

      // console.log(questions);

      questions.forEach((question) => {
        visitedArray.push({
          visited: false,
        });

        if (question.type === 'matching') {
          const matchingQuestionArray = [];
          question.q_matchings.forEach((matchings) => {
            if (matchings.student_answers !== null && matchings.student_answers.length != 0) {
              // console.log('matvchinggs');
              // console.log(matchings);
              matchingQuestionArray.push(matchings.student_answers[0].answerable_id);
            } else {
              matchingQuestionArray.push(-1);
            }
          });
          answerArray.push(matchingQuestionArray);
          // console.log('answerarry');
          // console.log(answerArray);
        } else if (question.type === 'cloze') {
          const clozeQuestionArray = [];
          // question.q_matchings.forEach((element) => {
          //   clozeQuestionArray.push(-1);
          // });

          if (question.student_answers === null || question.student_answers.length === 0) {
            // for (let i = 0; i < question.q_clozes_answer_hidden.length; i += 1) {
            //   clozeQuestionArray.push(null);
            // }
            question.q_clozes_answer_hidden.forEach((cloze) => {
              clozeQuestionArray.push(null);
            });
          } else {
            // for (let i = 0; i < question.q_clozes_answer_hidden.length; i += 1) {
            //   for (let j = 0; j < question.student_answers.length; j += 1) {
            //     if ()
            //   }
            //   clozeQuestionArray.push(null);
            // }

            // for (let i = 0; i < question.student_answers.length; i += 1) {
            //   for (let j = 0; j < question.q_clozes_answer_hidden.length; j += 1) {
            //     console.log('comparation');
            //     console.log(question.q_clozes_answer_hidden[j].id);
            //     console.log(question.student_answers[i].answerable_id);
            //     if (question.q_clozes_answer_hidden[j].id == question.student_answers[i].answerable_id) {
            //       console.log('answer.answer');
            //       console.log(question.student_answers.answer);
            //       clozeQuestionArray.push(question.student_answers.answer);
            //       break;
            //     }
            //     if (j === question.q_clozes_answer_hidden.length - 1) {
            //       clozeQuestionArray.push(null);
            //     }
            //   }
            //   // clozeQuestionArray.push(null);
            // }

            question.q_clozes_answer_hidden.forEach((answerHidden) => {
              let done = false;
              for (let i = 0; i < question.student_answers.length; i += 1) {
                if (question.student_answers[i].answerable_id == answerHidden.id) {
                  // console.log(answer.answer);
                  clozeQuestionArray.push(question.student_answers[i].answer);
                  done = true;
                  // continue;
                }
              }
              if (!done) {
                clozeQuestionArray.push(null);
              }
            });

            // question.student_answers.forEach((answer) => {
            //   let done = false;
            //   for (let i = 0; i < question.q_clozes_answer_hidden.length; i += 1) {
            //     console.log('comparation');
            //     console.log(i)
            //     console.log(question.q_clozes_answer_hidden.length - 1)
            //     // console.log(question.q_clozes_answer_hidden[i].id);
            //     // console.log(answer.answerable_id);
            //     // console.log('answer.answer');
            //     if (question.q_clozes_answer_hidden[i].id == answer.answerable_id) {
            //       // console.log(answer.answer);
            //       clozeQuestionArray.push(answer.answer);
            //       done = true
            //       continue;
            //     }
            //     if (i == question.q_clozes_answer_hidden.length - 1 && !done) {
            //       clozeQuestionArray.push(null);
            //     }
            //   }
            // });
          }
          // console.log('clozeQuestionArray')
          // console.log(clozeQuestionArray)
          answerArray.push(clozeQuestionArray);
        } else if (question.type === 'essay') {
          if (question.student_answers === null || question.student_answers.length !== 0) {
            answerArray.push(question.student_answers[0].answer);
          } else {
            answerArray.push(null);
          }
        } else if (question.student_answers === null || question.student_answers.length !== 0) {
          answerArray.push(question.student_answers[0].answerable_id);
        } else {
          answerArray.push(null);
        }
      });
      visitedSectionArray.push(visitedArray);
      answerSectionArray.push(answerArray);
      // console.log('answerSectionArray');
      // console.log(answerSectionArray);
    });
    return [visitedSectionArray, answerSectionArray];
  }

  render() {
    const {
      sectionIndex, questionIndex, loading, questionVisited, time, token,
    } = this.state;
    const { questions } = this.props;
    return (
      <div>
        {
          loading === false
          && (
            <Container className="d-flex flex-column p-4">
              <QuestionCard
                sectionIndex={sectionIndex}
                questionIndex={questionIndex}
                questionVisited={questionVisited}
                time={time}
                token={token}
              />
            </Container>
          )
        }
        {
          questions.length === 0
          && (
            <div className="spinner">
              <Spinner animation="grow" role="status" size="xl" className />
            </div>
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    questions, questionVisited, sectionIndex, questionIndex, profile, answering,
  } = state;
  return {
    questions, questionVisited, sectionIndex, questionIndex, profile, answering,
  };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    addQuestion, toggleAnswering,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(Exam);
