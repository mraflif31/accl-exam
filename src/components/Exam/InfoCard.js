import React, { Component } from 'react';
import './index.css';
import {
  Card, Image, Row, Col,
} from 'react-bootstrap';

export default class InfoCard extends Component {
  render() {
    return (
      <Card className="card3">
        <Card.Body className="card-body3 d-flex flex-row justify-content-around align-items-center flex-wrap">
          <Row>
            <Col className="d-flex justify-content-center align-items-center">
              <Image src={ this.props.src } className="study-logo" />
            </Col>
            <Col>
              <div className="d-flex flex-column align-items-start text-align-center">
                <p className="card3-p1">{ this.props.top}</p>
                <p className="card3-p2">{ this.props.middle }</p>
                <p className="card3-p3">{ this.props.bottom }</p>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}
