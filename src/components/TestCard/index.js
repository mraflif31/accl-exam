import React, { Component } from 'react';
import {
  Container, Row, Col, Spinner,
} from 'react-bootstrap';
import { AsyncStorage } from 'AsyncStorage';
import axios from 'axios';
import moment from 'moment';
import 'moment/locale/id';
import { connect } from 'react-redux';
import CardSchool from './CardSchool';
import './TestCard.css';
import Statement from './Statement';
import CardProfileLesson from './CardProfileLesson';
import CountDown from './CountDown';
import {API_URL} from "../../constants/api-domain";

class TestCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countdownModal: true,
      statementModal: false,
      hours: 0,
      minutes: 0,
      seconds: 5,
      spinner: false,
      loading: true,
      checked: false,
    };

    this.onBlur = this.onBlur.bind(this);
  }

  startCountdown() {
    const {
      hours, minutes, seconds, countdownModal, statementModal, spinner,
    } = this.state;
    if (hours <= 0 && minutes <= 0 && seconds <= 0) {
      clearInterval(this.countdown);
      // setTimeout(() => { this.setState({ spinner: true}) },1000);
      this.setState({
        spinner: true,
        countdownModal: !countdownModal,
      });
      setTimeout(() => {
        this.setState({
          statementModal: !statementModal,
          spinner: false,
        });
      }, 300);
    } else if (seconds > 0) {
      this.setState({ seconds: seconds - 1 });
    } else if (minutes > 0) {
      this.setState({ minutes: minutes - 1 });
      this.setState({ seconds: 59 });
    } else if (hours > 0) {
      this.setState({
        hours: hours - 1,
        minutes: 59,
        seconds: 59,
      });
    }
  }

  async componentDidMount() {
    document.addEventListener('blur', this.onBlur);
    document.addEventListener('keydown', this.onInspectElement);
    document.addEventListener('contextmenu', this.onContextMenu);
    moment.locale('id');
    this.countdown = setInterval(() => { this.startCountdown(); }, 1000);
    await AsyncStorage.getItem('profile')
      .then((val) => {
        let profile = JSON.parse(val);
        axios.post(`${API_URL}/api/exam/check-approve`, {
          token: profile.session_token,
        })
          .then((res) => {
            this.setState({ checked: res.data.message, token: profile.session_token });
          });
        axios.post(`${API_URL}/api/exam/card`, {
          user_id: profile.user_id,
          exam_id: profile.exam.id,
        })
          .then((res) => {
            if (res.status === 200) {
              let waiting_time = new Date(1000 * res.data.waiting_time).toISOString();
              this.setState({
                school_name: res.data.institution.name,
                school_image: res.data.institution.image,
                school_npsn: res.data.institution.npsn,
                school_accreditation: res.data.institution.accreditation,
                school_foundation: res.data.institution.foundation,
                class_code: res.data.user.class_code,
                fullname: res.data.user.fullname,
                nik: res.data.user.nik,
                self_image: res.data.user.self_image,
                exam_type: res.data.exam.exam_type,
                exam_date: res.data.exam.date,
                subject: res.data.exam.subject,
                subject_image: res.data.exam.subject_image,
                school_year: res.data.exam.school_year,
                hours: parseInt(waiting_time.substr(11, 2), 10),
                minutes: parseInt(waiting_time.substr(14, 2), 10),
                seconds: parseInt(waiting_time.substr(17, 2), 10),
                code: profile.exam.code,
                loading: false,
                status: res.data.status,
              });
              if (!this.state.status) {
                clearInterval(this.countdown);
                // setTimeout(() => { this.setState({ spinner: true}) },1000);
                this.setState({
                  spinner: true,
                  countdownModal: !this.state.countdownModal,
                });
                setTimeout(() => {
                  this.setState({
                    statementModal: !this.state.statementModal,
                    spinner: false,
                  });
                }, 300);
              }
            }
            if (res.status === 202) {
              if (res.data.error === 'Institution Not Found') {
                alert('Institution Not Found');
              }
              if (res.data.error === 'Exam Not Found') {
                alert('Exam Not Found');
              }
              if (res.data.error === 'User Not Found') {
                alert('User Not Found');
              }
              if (res.data.error === 'Create Waiting Time Failed') {
                alert('Create Waiting Time Failed');
              }
            }
          })
          .catch((error) => {
            console.log(error);
          });
      });
  }

  componentWilUnmount() {
    document.removeEventListener('blur', this.onBlur);
    document.removeEventListener('keydown', this.onInspectElement);
    document.removeEventListener('contextmenu', this.onContextMenu);
  }

  onInspectElement(event) {
    if ((event.ctrlKey && event.shiftKey && event.keyCode === 73) || event.keyCode === 123) {
      event.preventDefault();
    }
  }

  onContextMenu(e) {
    e.preventDefault();
    return false;
  }

  onBlur() {
  }

  spinner() {
    return (this.state.spinner);
  }

  renderStatement() {
    if (this.spinner()) {
      return (
        <Col>
          <Spinner animation="grow" />
        </Col>
      );
    }
    return (
      <Col>
        <Statement checked={this.state.checked} token={this.state.token} />
      </Col>
    );
  }

  render() {
    const { statementModal, countdownModal, spinner, loading } = this.state;
    return (
      <div>
        {
          loading === false
          && (
            <Container>
              <Row className="no-margin">
                <Col>
                  <CardSchool
                    src={require('../../assets/images/logo_egs.png')}
                    foundation={this.state.school_foundation}
                    school={this.state.school_name}
                    npsn={this.state.school_npsn}
                    accreditation={this.state.school_accreditation}
                  />
                </Col>
              </Row>
              <Row className="no-margin">
                <Col xs={12} md={6} lg={6} xl={6} className="d-flex">
                  <CardProfileLesson
                    src={require('../../assets/images/empty_photo.png')}
                    top={this.state.class_code}
                    middle={this.state.fullname}
                    bottom={`NISN: ${this.state.nik}`}
                  />
                </Col>
                <Col xs={12} md={6} lg={6} xl={6} className="d-flex">
                  <CardProfileLesson
                    src={require('../../assets/images/empty_matpel.png')}
                    top={`${this.state.exam_type} (${this.state.school_year})`}
                    middle={this.state.subject}
                    bottom={moment(this.state.exam_date).format('dddd, Do MMMM YYYY')}
                  />
                </Col>
              </Row>
              <Row className={statementModal ? 'no-margin' : 'no-margin d-none'}>
                {this.renderStatement()}
              </Row>
              <Row className={countdownModal ? 'no-margin' : 'no-margin d-none'}>
                <Col>
                  <CountDown
                    src={require('../../assets/images/logo_egs.png')}
                    hours={this.state.hours}
                    minutes={this.state.minutes}
                    seconds={this.state.seconds}
                  />
                </Col>
              </Row>
              <p className="text-center" id="token-p">
                Token: {this.state.code}
              </p>
            </Container>
          )
        }
        {
          loading === true
          && (
            <div className="spinner">
              <Spinner animation="grow" role="status" size="xl" className />
            </div>
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { profile } = state;
  return { profile };
};

export default connect(mapStateToProps)(TestCard);
