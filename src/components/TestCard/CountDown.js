import React, { Component } from 'react';
import { Row, Card, Col } from 'react-bootstrap';
import './TestCard.css';

class CountDown extends Component {
  constructor(props){
    super(props);
  }

  pad(n, size = 0){
    var s = String(n);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }

  timerWarning(){
    return (this.props.hours === 0 && this.props.minutes === 0 && this.props.seconds <= 30);
  }

  render() {
    return (
      <Card className="my-2 card-color">
        <Card.Body className="card-color-body">
          <Row className="no-margin">
            <Col style={{ alignSelf: 'center' }} xs={3}>
              <img
                src={this.props.src}
                className="img-fluid img-logo"
              />
            </Col>
            <Col xs={9} style={{ alignSelf: 'center' }}>
              <Row className="no-margin">
                <Col className="text-left">
                  <p className="card-1-top">Waktu Menuju Ujian:</p>
                </Col>
              </Row>
              <Row className="no-margin">
                <Col className="text-left">
                  <p className={ this.timerWarning() ? 'text-warning card-1-middle' : 'card-1-middle'}>{this.pad(this.props.hours)} : {this.pad(this.props.minutes)} : {this.pad(this.props.seconds)}</p>
                </Col>
              </Row>
              <Row className="no-margin">
                <Col className="text-left">
                  <p className="card-1-bottom">
                    Periksa lagi datamu, jika ada kesalahan, laporkan pada pengawas
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

export default CountDown;
