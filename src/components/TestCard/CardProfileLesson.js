import React, { Component } from 'react';
import { Row, Card, Col } from 'react-bootstrap';
import './TestCard.css';

class CardProfileLesson extends Component {
  render() {
    return (
      <Col className="no-padding my-2">
        <Card className="card-color h-100">
          <Card.Body className="card-color-body align-items-center d-flex">
            <Row className="no-margin align-items-center w-100">
              <Col className="text-right d-inline-flex align-items-center" xs={4}>
                <img
                  src={this.props.src}
                  className="rounded-circle rounded-image"
                />
              </Col>
              <Col xs={8}>
                <Row className="no-margin">
                  <Col className="text-left">
                    <p className="card-1-top">{this.props.top}</p>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col className="text-left">
                    <p className="card-1-middle">{this.props.middle}</p>
                  </Col>
                </Row>
                <Row className="no-margin">
                  <Col className="text-left">
                    <p className="card-1-bottom">{this.props.bottom}</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
    );
  }
}

export default CardProfileLesson;
