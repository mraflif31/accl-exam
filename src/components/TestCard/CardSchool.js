import React, { Component } from 'react';
import { Row, Card, Col } from 'react-bootstrap';
import './TestCard.css';

class CardSchool extends Component {
  render() {
    return (
      <Card className="card-color my-2">
        <Card.Body className="card-color-body">
          <Row className="no-margin">
            <Col style={{ alignSelf: 'center' }} xs={3}>
              <img
                src={this.props.src}
                className="img-fluid img-logo"
              />
            </Col>
            <Col xs={6} style={{alignSelf: 'center'}}>
              <Row className="no-margin">
                <Col className="text-left">
                  <p className="card-1-top">{this.props.foundation}</p>
                </Col>
              </Row>
              <Row className="no-margin">
                <Col className="text-left">
                  <p className="card-1-middle">{this.props.school}</p>
                </Col>
              </Row>
              <Row className="no-margin">
                <Col className="text-left">
                  <p className="card-1-bottom">NPSN: {this.props.npsn}</p>
                </Col>
              </Row>
            </Col>
            <Col xs={3} className="no-padding" style={{ alignSelf: 'center' }}>
              <Row className="no-margin">
                <Col>
                  <p id="akreditasi" className="text-center">Akreditasi</p>
                </Col>
              </Row>
              <Row className="no-margin">
                <Col>
                  <p id="akreditasi-index" className="display-3 text-center">{this.props.accreditation}</p>
                </Col>
              </Row>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

export default CardSchool;
