import React, { Component } from 'react';
import { Row, Card, Col } from 'react-bootstrap';
import './TestCard.css';
import {
  Redirect,
  Route,
} from 'react-router-dom';
import axios from 'axios';
import { connect } from 'react-redux';
import { AsyncStorage } from 'AsyncStorage';
import { bindActionCreators } from 'redux';
import { saveProfile } from '../../actions';
import { API_URL } from '../../constants/api-domain';

class Statement extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: props.checked,
    };
  }

  onApprove() {
    axios.post(`${API_URL}/api/exam/update-approve`, {
      token: this.props.token,
    }).then((res) => {
      if (res.status === 200) {
        this.setState({ checked: true });
      }
    });
  }

  render() {
    if (this.state.checked) {
      return (
        <Redirect to="/exam" />
      );
    }
    return (
      <Card className="my-2 card-color text-center">
        <Card.Body>
          <Row>
            <Col className="text-center statement-header">Pernyataan Kejujuran</Col>
          </Row>
          <Row>
            <Col className="text-center mt-1">
              <input type="checkbox" className="form-check-input" id="check-confidence" onChange={() => this.onApprove()} />
              <label className="form-control-check" htmlFor="exampleCheck1">
                Saya berjanji akan mengerjakan ujian dengan jujur
              </label>
            </Col>
          </Row>
          <Row className="no-margin" style={{ fontSize: '12px' }}>
            <Col className="text-center font-italic">
              Ujian tidak akan dimulai sebelum kamu menyetujui pernyataan diatas
            </Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  const { profile } = state;
  return { profile };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    saveProfile,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps())(Statement);
