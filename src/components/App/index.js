import React, { Component } from 'react';
import './App.css';
import {
  Redirect,
  Route,
} from 'react-router-dom';
import { AsyncStorage } from 'AsyncStorage';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Container, Spinner } from 'react-bootstrap';
import { saveProfile } from '../../actions';
import { API_URL } from '../../constants/api-domain';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nik: null,
      password: null,
      code: null,
      testcard: false,
      exam: false,
      alertDis: 'none',
      textAlert: 'Username atau Password salah',
      loading: true,
    };
  }

  async componentDidMount() {
    await AsyncStorage.getItem('profile')
      .then((val) => {
        // console.log(val);
        if (val == null || val == 'null') {
          this.setState({ loading: false });
        } else {
          const profile = JSON.parse(val);
          if (profile.session_token != null) {
            axios.post(`${API_URL}/api/exam/check-approve`, {
              token: profile.session_token,
            })
              .then((res) => {
                if (res.status === 200) {
                  this.setState({ exam: true, loading: false });
                }
                if (res.status === 202) {
                  this.setState({ testcard: true, loading: false });
                }
              });
          } else if (profile.session_token == null) {
            console.log('Token Expired');
          }
        }
      }).catch((e) => console.log(e));
    document.addEventListener('blur', this.onBlur);
    document.addEventListener('keydown', this.onInspectElement);
    document.addEventListener('contextmenu', this.onContextMenu);
  }

  componentWilUnmount() {
    document.removeEventListener('blur', this.onBlur);
    document.removeEventListener('keydown', this.onInspectElement);
    document.removeEventListener('contextmenu', this.onContextMenu);
  }

  onInspectElement(event) {
    if ((event.ctrlKey && event.shiftKey && event.keyCode === 73) || event.keyCode === 123) {
      event.preventDefault();
    }
  }

  setProfile() {
    const { saveProfile } = this.props;
    AsyncStorage.getItem('profile')
      .then(async (value) => {
        const data = JSON.parse(value);
        const result = {
          profile: {
            userId: data.user_id,
            exam: data.exam,
            remainingTime: data.remaining_time,
            sessionToken: data.session_token,
          },
        };
        saveProfile(result);
      });
  }

  onSubmit() {
    const { nik, password, code } = this.state;
    axios.post(`${API_URL}/api/exam/login`, {
      nik,
      password,
      exam_code: code,
    })
      .then(async (res) => {
        if (res.status === 200) {
          await AsyncStorage.setItem('profile', JSON.stringify(res.data));
          await this.setProfile();
          this.setState({ testcard: true });
        }
        if (res.status === 202) {
          if (res.data.error === 'NIK or Password Invalid') {
            this.setState({ alertDis: 'block' });
          }
          if (res.data.error === 'Exam Not Found') {
            this.setState({ alertDis: 'block', textAlert: 'Ujian tidak ditemukan' });
          }
          if (res.data.message === 'Exam Expired') {
            this.setState({ alertDis: 'block', textAlert: 'Ujian kadaluarsa' });
          }
          if (res.data.error === 'School Grade Invalid') {
            this.setState({ alertDis: 'block', textAlert: 'Ujian ini bukan untuk kelas Anda!' });
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleChange({ value }, type) {
    this.setState({ [type]: value });
  }

  onContextMenu(e) {
    e.preventDefault();
    return false;
  }

  render() {
    if (this.state.testcard) {
      return (<Redirect to="/test-card" />);
    }
    if (this.state.exam) {
      return (<Redirect to="/exam" />);
    }
    return (
      <div onContextMenu={(e) => e.preventDefault()} className="App">
        {
          this.state.loading === false
          && (
            <div>
              <div>
                <header className="App-header">
                  <img src={require('../../assets/images/draftlogo_2.png')} className="logo" alt="logo" />
                </header>
                <body className="App-body text-center row">
                  <div className="col-12 ">
                    <p className="alert alert-danger font-weight-bold" style={{ display: this.state.alertDis }}>
                      {this.state.textAlert}
                    </p>
                    <form className="form border rounded p-3 px-4 mb-3">
                      <div className="form-div">
                        <div className="form-group">
                          <label className="label">Nomor Induk</label>
                          <div>
                            <input
                              placeholder="Contoh: 12345"
                              className="form-control"
                              type="text"
                              name="name"
                              value={this.state.nik}
                              onChange={({ target }) => {
                                this.handleChange(target, 'nik');
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="label">Password</label>
                          <div>
                            <input
                              placeholder="*******"
                              className="form-control"
                              type="password"
                              name="password"
                              value={this.state.password}
                              onChange={({ target }) => {
                                this.handleChange(target, 'password');
                              }}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="label">Token Soal</label>
                          <div>
                            <input
                              placeholder="Contoh: 3A42V"
                              className="form-control"
                              type="text"
                              name="token"
                              value={this.state.code}
                              onChange={({ target }) => {
                                this.handleChange(target, 'code');
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      {/* <Button className="submit-button" variant="warning">Masuk</Button> */}
                      <input className="submit-button p-2 mb-1" type="button" value="Masuk" onClick={() => this.onSubmit()} />
                    </form>
                  </div>
                </body>
              </div>
              <div className="trademark">
                <p className="mb-1">accl education integrated system</p>
                <p>2020</p>
              </div>
            </div>
          )
        }
        {
          this.state.loading === true
          && (
            <div className="spinner">
              <Spinner animation="grow" role="status" size="xl" className />
            </div>
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { profile } = state;
  return { profile };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    saveProfile,
  },
  dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(App);
